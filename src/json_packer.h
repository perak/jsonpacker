/**
 * JSONPacker class. Packs JSON into binary representation and vice versa.
 */

#ifndef JSON_PACKER_H
#define JSON_PACKER_H

#include <string>

// No need for class here - but who knows, in the future...

class JSONPacker {
    public:
        JSONPacker() {};
        ~JSONPacker() {};

        /**
         * Packs JSON file into binary representation
         *
         * @param sInputFile path to input JSON file.
         * @param sOutputFile path where output binary file will be written.
         */
        static void Pack(std::string &sInputFile, std::string &sOutputFile);

        /**
         * Unpacks binary representation into JSON
         *
         * @param sInputFile path to input binary file.
         * @param sOutputFile path where output JSON file will be written.
         */
        static void Unpack(std::string &sInputFile, std::string &sOutputFile);
};

#endif //JSON_PACKER_H
