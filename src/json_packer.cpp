#include "json_packer.h"

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include <fstream>
#include <map>

// Data types
#define DT_NULL 0
#define DT_FALSE 1
#define DT_TRUE 2
#define DT_OBJECT 3
#define DT_ARRAY 4
#define DT_STRING 5
#define DT_INTEGER 6
#define DT_DOUBLE 7


void JSONPacker::Pack(std::string &sInputFile, std::string &sOutputFile) {
    // Open files
    std::ifstream in_file(sInputFile);
    std::ofstream out_file(sOutputFile, std::ios::binary);
    std::string dict_filename = sOutputFile + ".dict";
    std::ofstream dict_file(dict_filename, std::ios::binary);

    // Dictionary for storing keys
    std::map<std::string, uint32_t> dictionary;

    // Read file line by line. Each line represents JSON object
    std::string line;
    while(std::getline(in_file, line)) {
        // Parse JSON
        rapidjson::Document json;
        json.Parse(line.c_str());

        // Write member count
        auto member_count = (uint32_t)json.MemberCount();
        out_file.write((char*)(&member_count), sizeof(uint32_t));

        // Iterate over JSON members
        for(rapidjson::Value::ConstMemberIterator itr = json.MemberBegin(); itr != json.MemberEnd(); ++itr) {
            // Register key into dictionary
            std::string key_name = itr->name.GetString();

            uint32_t key_code;
            if(dictionary.find(key_name) == dictionary.end()) {
                // We have a new dictionary entry
                key_code = (uint32_t)dictionary.size();
                dictionary[key_name] = key_code;

                // Write integer representation of the key into dictionary
                dict_file.write((char*)(&key_code), sizeof(uint32_t));

                // Write length of the string
                auto key_len = (uint32_t)key_name.size();
                dict_file.write((char*)(&key_len), sizeof(uint32_t));

                // Write string (without trailing zero)
                dict_file.write(key_name.c_str(), key_len);
            } else {
                key_code = dictionary[key_name];
            }

            // Write integer representation of the key
            out_file.write((char*)(&key_code), sizeof(uint32_t));

            // Get data type
            uint8_t data_type = 0;
            switch(itr->value.GetType()) {
                case rapidjson::kNullType: data_type = DT_NULL; break;
                case rapidjson::kFalseType: data_type = DT_FALSE; break;
                case rapidjson::kTrueType: data_type = DT_TRUE; break;
                case rapidjson::kObjectType: data_type = DT_OBJECT; break;
                case rapidjson::kArrayType: data_type = DT_ARRAY; break;
                case rapidjson::kStringType: data_type = DT_STRING; break;
                case rapidjson::kNumberType: {
                    if(itr->value.IsInt64()) {
                        data_type = DT_INTEGER;
                    } else {
                        data_type = DT_DOUBLE;
                    }
                } break;
            }
            // Write datatype. We have only 7 types (minus 2 ignored types), so 1 byte is more than enough
            out_file.write((char*)(&data_type), sizeof(uint8_t));

            // Write value
            switch(data_type) {
                //
                // For "null", "false" and "true": we already wrote datatype without length and content - and that is enough
                // Note: "object" and "array" keys are ignored (by task requirements)
                //
                case DT_STRING: {
                    std::string data_value = itr->value.GetString();
                    auto data_len = (uint32_t)data_value.size();

                    // Write length of the string
                    out_file.write((char*)(&data_len), sizeof(uint32_t));

                    // Write string (without trailing zero)
                    out_file.write(data_value.c_str(), data_len);
                } break;

                case DT_INTEGER: {
                    // Integer
                    int64_t data_value = itr->value.GetInt64();
                    // Write integer
                    out_file.write((char *) (&data_value), sizeof(int64_t));
                } break;

                case DT_DOUBLE: {
                    double data_value = itr->value.GetDouble();
                    // Write double (should be 8 bytes both on 64-bit and 32-bit...)
                    out_file.write((char*)(&data_value), sizeof(double));
                } break;

                default: ;
            }
        }
    }

    dict_file.close();
    out_file.close();
    in_file.close();
}

void JSONPacker::Unpack(std::string &sInputFile, std::string &sOutputFile) {
    // Open files
    std::ifstream in_file(sInputFile, std::ios::binary);

    std::string dict_filename = sInputFile + ".dict";
    std::ifstream dict_file(dict_filename, std::ios::binary);

    std::ofstream out_file(sOutputFile);

    // Load dictionary from file
    std::map<uint32_t, std::string> dictionary;
    while(dict_file.peek() != std::istream::traits_type::eof()) {
        // Read integer representation of the key
        uint32_t key_code;
        dict_file.read((char*)(&key_code), sizeof(uint32_t));

        // Read length of the key name
        uint32_t key_len;
        dict_file.read((char*)(&key_len), sizeof(uint32_t));

        std::string key_name;
        key_name.resize(key_len);

        // Hope that string's internal buffer is stored in a single continuous block
        dict_file.read(&key_name[0], key_len);

        dictionary[key_code] = key_name;
    }

    while(in_file.peek() != std::istream::traits_type::eof()) {
        // Read member count
        uint32_t member_count;
        in_file.read((char*)(&member_count), sizeof(uint32_t));

        // Read record and construct json object
        rapidjson::StringBuffer string_buffer;
        rapidjson::Writer<rapidjson::StringBuffer> json_writer(string_buffer);
        json_writer.StartObject();
        while(member_count--) {
            // Read integer representation of the key
            uint32_t key_code;
            in_file.read((char*)(&key_code), sizeof(uint32_t));

            // Get key name from dictionary
            std::string key_name = dictionary[key_code];
            json_writer.Key(key_name.c_str());

            // Read data type
            uint8_t data_type;
            in_file.read((char*)(&data_type), sizeof(uint8_t));

            // Get value
            switch(data_type) {
                //
                // For datatypes "null", "false" and "true" we know value - it is not stored into file
                //
                case DT_NULL: json_writer.Null(); break;
                case DT_FALSE: json_writer.Bool(false); break;
                case DT_TRUE: json_writer.Bool(true); break;

                // "Object" and "Array" keys are ignored (by task requirements)
                // case DT_OBJECT: ; break;
                // case DT_ARRAY: ; break;

                // String
                case DT_STRING: {
                    // Read length of the string
                    uint32_t data_len;
                    in_file.read((char*)(&data_len), sizeof(uint32_t));

                    // Read string
                    std::string data_value;
                    data_value.resize(data_len);
                    in_file.read(&data_value[0], data_len);

                    json_writer.String(data_value.c_str());
                } break;

                case DT_INTEGER: {
                    // Read integer
                    int64_t data_value;
                    in_file.read((char*)(&data_value), sizeof(int64_t));

                    json_writer.Int64(data_value);
                } break;

                case DT_DOUBLE: {
                    // Read double (should be 8 bytes both on 64-bit and 32-bit...)
                    double data_value;
                    in_file.read((char*)(&data_value), sizeof(double));

                    json_writer.Double(data_value);
                } break;

                default: ;
            }

        }
        json_writer.EndObject();

        // Write record to output file
        std::string json_string = string_buffer.GetString();
        json_string += "\n";
        out_file.write(json_string.c_str(), json_string.size());
    }

    out_file.close();
    dict_file.close();
    in_file.close();
}
