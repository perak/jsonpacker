# JSON Packer/Unpacker

## Compile

Requires `cmake` and compiler with C++11 support.

Just run `build.sh` script from project root and you will find executable in `bin/json_packer`.

Compiles on Linux and Mac. Not sure if it will compile on Windows out of the box.

## Usage
```bash
USAGE: 

   json_packer  [-h] [--version] {-p <string>|-u <string>} -o <string>


Where: 

   -o <string>,  --output <string>
     (required) Output file

   One of:
      -p <string>,  --pack <string>
        JSON file to pack

      -u <string>,  --unpack <string>
        Binary file to unpack

   --,  --ignore_rest
     Ignores the rest of the labeled arguments following this flag.

   --version
     Displays version information and exits.

   -h,  --help
     Displays usage information and exits.

```

## Packer

### Input

Input is text file in following format:

```json
{ "key1": "value", "key2":42, "key3":true}
{ "sadsf": "dsewtew", "dsre": 3221, "sdfds": "dsfewew" }
{ "name": "John Doe", "age": 30, "height": 1.85, "balance": null }
```
Each line is a record represented as a JSON object with basic members (object and array members are ignored).

Packer converts file into compact binary format.

### Output

Output is two binary files:

- dictionary
- data

### Example:

Use `-p` switch to pack file:
```bash
json_packer -p original.json -o packed.bin
```

Output is two files:
```bash
packed.bin
packed.bin.dict
```

## Unpacker

### Input

Input is binary data and dict files.

### Output

Output is text file containing records. Each line is a record represented as a JSON object (see Packer input above).

### Example

Use `-u` switch to unpack binary files:
```bash
json_packer -u packed.bin -o unpacked.json
```
