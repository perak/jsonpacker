/**
 * JSON Packer CLI
 */

#include <iostream>

#include "json_packer.h"
#include "tclap/CmdLine.h"


int main(int argc, char** argv) {
    //
    // Parse command line arguments
    //
    TCLAP::CmdLine cmd("JSON packer", ' ', "1.0");
    TCLAP::ValueArg <std::string> pack_file_arg("p", "pack", "JSON file to pack", false, "", "string");
    TCLAP::ValueArg <std::string> unpack_file_arg("u", "unpack", "Binary file to unpack", false, "", "string");

    TCLAP::OneOf input;
    input.add(pack_file_arg).add(unpack_file_arg);
    cmd.add(input);

    TCLAP::ValueArg <std::string> output_file_arg("o", "output", "Output file", true, "", "string");
    cmd.add(output_file_arg);

    try {
        cmd.parse(argc, argv);
    } catch(TCLAP::ArgException &e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    }

    std::string output_filename = output_file_arg.getValue();

    //
    // Do pack/unpack
    //
    JSONPacker json_packer;
    if(pack_file_arg.isSet()) {
        // Pack
        std::string input_filename = pack_file_arg.getValue();
        json_packer.Pack(input_filename, output_filename);
    } else if(unpack_file_arg.isSet()) {
        // Unpack
        std::string input_filename = unpack_file_arg.getValue();
        json_packer.Unpack(input_filename, output_filename);
    }

    return 0;
}
